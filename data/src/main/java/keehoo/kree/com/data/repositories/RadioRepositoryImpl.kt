package keehoo.kree.com.data.repositories

import io.reactivex.Single
import keehoo.kree.com.data.api.SwedishRadioApi
import keehoo.kree.com.domain.entities.SwedishRadioPage
import keehoo.kree.com.domain.repositories.RadioProgramRepository

class RadioRepositoryImpl(private val api: SwedishRadioApi) : RadioProgramRepository {

    override fun getPageOfSwedishRadio(pageNumber: Int): Single<SwedishRadioPage> {
        println("TAGTAG requesting from api  ${pageNumber}")

        return api.getAllSwedishRadio(page = pageNumber)
    }
}
