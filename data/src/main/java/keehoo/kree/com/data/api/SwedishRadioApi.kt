package keehoo.kree.com.data.api

import io.reactivex.Single
import keehoo.kree.com.domain.entities.SwedishRadioPage
import retrofit2.http.GET
import retrofit2.http.Query

interface SwedishRadioApi {

    @GET("programs/")
    fun getAllSwedishRadio(@Query("format") format: String = "json",
                           @Query("size") size: Int = 40,
                           @Query("page") page: Int = 1): Single<SwedishRadioPage>
}
