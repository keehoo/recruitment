package keehoo.kree.com.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import keehoo.kree.com.data.daos.ExportDao
import keehoo.kree.com.data.entities.ExportEntity

@Database(entities = [ExportEntity::class], version = 1)
abstract class AppDataBase : RoomDatabase() {

    abstract fun exportDao(): ExportDao
}