package keehoo.kree.com.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ExportEntity(
    @PrimaryKey val id: Long?,
    val exportName: String,
    val exportDate: String,
    val exportTime: String,
    val exportUser: String,
    val exportLocal: String
)