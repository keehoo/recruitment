package keehoo.kree.com.data.daos

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.Single
import keehoo.kree.com.data.entities.ExportEntity

@Dao
interface ExportDao {

    @Query("SELECT * FROM ExportEntity")
    fun getAll(): Single<List<ExportEntity>>

//    @Query("SELECT * FROM user WHERE first_name LIKE :first AND " +
//        "last_name LIKE :last LIMIT 1")
//    fun findByName(first: String, last: String): ExportEntity

    @Insert
    fun insertAll(vararg users: ExportEntity)

    @Delete
    fun delete(user: ExportEntity)
}