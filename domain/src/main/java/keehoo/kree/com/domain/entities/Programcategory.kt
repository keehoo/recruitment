package keehoo.kree.com.domain.entities

import java.io.Serializable

data class Programcategory(
    val id: Int,
    val name: String
): Serializable