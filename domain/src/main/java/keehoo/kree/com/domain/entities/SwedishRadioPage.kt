package keehoo.kree.com.domain.entities

import com.google.gson.annotations.SerializedName

data class SwedishRadioPage(
        @SerializedName("copyright")
        val copyright: String,
        @SerializedName("pagination")
        val pagination: Pagination,
        @SerializedName("programs")
        val programs: ArrayList<Program>
)