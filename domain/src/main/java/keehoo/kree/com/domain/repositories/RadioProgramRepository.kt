package keehoo.kree.com.domain.repositories

import io.reactivex.Single
import keehoo.kree.com.domain.entities.SwedishRadioPage

interface RadioProgramRepository {

    fun getPageOfSwedishRadio(pageNumber: Int = 1): Single<SwedishRadioPage>

}