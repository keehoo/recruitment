package keehoo.kree.com.domain.entities

import java.io.Serializable

data class Channel(
    val id: Int,
    val name: String
): Serializable