package keehoo.kree.com.domain.entities

import java.io.Serializable

data class Socialmediaplatform(
    val platform: String,
    val platformurl: String
): Serializable