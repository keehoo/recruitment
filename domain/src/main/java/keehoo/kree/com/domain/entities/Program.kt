package keehoo.kree.com.domain.entities

import java.io.Serializable

data class Program(
        val archived: Boolean,
        val broadcastinfo: String,
        val channel: Channel,
        val description: String,
        val email: String,
        val hasondemand: Boolean,
        val haspod: Boolean,
        val id: Int,
        val name: String,
        val phone: String,
        val programcategory: Programcategory,
        val programimage: String,
        val programimagetemplate: String,
        val programimagetemplatewide: String,
        val programimagewide: String,
        val programslug: String,
        val programurl: String,
        val responsibleeditor: String,
        val socialimage: String,
        val socialimagetemplate: String,
        val socialmediaplatforms: ArrayList<Socialmediaplatform>
) : Serializable