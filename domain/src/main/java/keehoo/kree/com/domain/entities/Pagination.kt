package keehoo.kree.com.domain.entities

data class Pagination(
    val nextpage: String,
    val page: Int,
    val size: Int,
    val totalhits: Int,
    val totalpages: Int
)