package keehoo.kree.com.domain.usecases

import io.reactivex.Single
import keehoo.kree.com.domain.entities.SwedishRadioPage
import keehoo.kree.com.domain.repositories.RadioProgramRepository

//
//class GetAllExportsListUseCase(private val repository: ExportRepository) {
//
//    fun execute(): Single<List<Export>> {
//        return repository.getAllOrders()
//    }
//}

class GetAllRadioProgramsUseCase(private val repository: RadioProgramRepository) {

    fun execute(pageNumber: Int): Single<SwedishRadioPage> {
        return repository.getPageOfSwedishRadio(pageNumber)
    }
}