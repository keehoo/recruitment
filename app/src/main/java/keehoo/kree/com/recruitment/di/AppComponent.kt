package keehoo.kree.com.recruitment.di

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import keehoo.kree.com.recruitment.App
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    AndroidSupportInjectionModule::class,
    NetworkModule::class,
    MainAppModule::class,
    MainScreenModule::class]
)
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Factory<App> {

        override fun create(application: App): AppComponent {
            seedApplication(application)
            return build()
        }

        @BindsInstance
        internal abstract fun seedApplication(application: App)

        internal abstract fun build(): AppComponent
    }
}