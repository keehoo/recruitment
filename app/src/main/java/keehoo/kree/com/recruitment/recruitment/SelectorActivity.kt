package keehoo.kree.com.recruitment.recruitment

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import keehoo.kree.com.recruitment.R
import keehoo.kree.com.recruitment.recruitment.mainScreen.view.MainActivity
import kotlinx.android.synthetic.main.activity_selector.*

class SelectorActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selector)

        hackberry.setOnClickListener { MainActivity.start(this) }
    }
}
