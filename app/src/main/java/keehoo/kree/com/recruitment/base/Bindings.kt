package keehoo.kree.com.recruitment.base

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso
import keehoo.kree.com.recruitment.R

object Bindings {

    @JvmStatic
    @BindingAdapter("visibility")
    fun visibility(view: View, visible: Boolean?) {
        view.visibility = if (visible == true) View.VISIBLE else View.GONE
    }

    @JvmStatic
    @BindingAdapter("src")
    fun setImageDrawable(view: ImageView, drawable: Drawable) {
        view.setImageDrawable(drawable)
    }

    @JvmStatic
    @BindingAdapter(value = ["imageUrl"])
    fun setImage(view: ImageView, imageUrl: String?) {
        if (!imageUrl.isNullOrBlank())
            Picasso
                    .get()
//            .apply { setIndicatorsEnabled(BuildConfig.DEBUG) }
                    .load(imageUrl)
                    .error(ContextCompat.getDrawable(view.context, R.drawable.banana)!!)
                    .placeholder(ContextCompat.getDrawable(view.context, R.drawable.banana)!!)
//                    .placeholder(ContextCompat.getColor(view.context, R.color.white))
                    .stableKey(imageUrl)
//            .fit()
                    .into(view)
    }
}
