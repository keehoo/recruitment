package keehoo.kree.com.recruitment.recruitment.mainScreen.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import keehoo.kree.com.domain.entities.Program
import keehoo.kree.com.recruitment.R
import keehoo.kree.com.recruitment.base.ActivityBase
import keehoo.kree.com.recruitment.databinding.ActivityItemListBinding
import keehoo.kree.com.recruitment.recruitment.mainScreen.viewmodel.MainScreenViewModel
import keehoo.kree.com.recruitment.recruitment.programdetailsscreen.ProgramDetailsActivity
import kotlinx.android.synthetic.main.activity_item_list.*
import javax.inject.Inject


class MainActivity : ActivityBase() {

    private lateinit var vm: MainScreenViewModel

    @Inject
    lateinit var listAdapter: RadioAdapter

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        vm = viewModelOf(MainScreenViewModel::class)

        DataBindingUtil.setContentView<ActivityItemListBinding>(
                this,
                R.layout.activity_item_list)
                .also {
                    it.lifecycleOwner = this@MainActivity
                    it.viewModel = vm
                }

        hookUpAdapterFuncionality()

        vm.latestDataSet.observe(this, Observer  {
            it?.let {
                listAdapter.setData(it)
                listAdapter.onClick = { selectedProgram, sharedView, sharedTransitionName ->
                    handleRadioProgramSelected(
                            selectedProgram,
                            sharedView,
                            sharedTransitionName
                    )
                }
            }
        })
    }

    private fun hookUpAdapterFuncionality() {
        radioList.adapter = listAdapter
        radioList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1)) {
                    vm.loadUpNextPage()
                }
            }
        })
    }

    private fun handleRadioProgramSelected(program: Program, sharedView: View, transitionName: String) {
        val options: ActivityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                sharedView,
                transitionName)
        ProgramDetailsActivity.start(this, program, options.toBundle(), transitionName)
    }

    companion object {
        fun start(activity: Activity) {
            activity.startActivity(Intent(activity, MainActivity::class.java))
        }
    }
}
