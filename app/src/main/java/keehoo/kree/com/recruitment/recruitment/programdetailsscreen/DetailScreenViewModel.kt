package keehoo.kree.com.recruitment.recruitment.programdetailsscreen

import androidx.lifecycle.ViewModel
import com.kubickiengineering.ktx.mutableLiveData
import keehoo.kree.com.domain.entities.Program
import javax.inject.Inject

class DetailScreenViewModel @Inject constructor() : ViewModel() {

    var program = mutableLiveData<Program?>(null)
}