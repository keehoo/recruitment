package keehoo.kree.com.recruitment.di

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap
import keehoo.kree.com.recruitment.App
import keehoo.kree.com.recruitment.base.ViewModelFactory
import keehoo.kree.com.recruitment.recruitment.mainScreen.view.MainActivity
import keehoo.kree.com.recruitment.recruitment.mainScreen.viewmodel.MainScreenViewModel
import keehoo.kree.com.recruitment.recruitment.programdetailsscreen.DetailScreenViewModel
import keehoo.kree.com.recruitment.recruitment.programdetailsscreen.ProgramDetailsActivity
import javax.inject.Singleton

@Module
internal abstract class MainAppModule {

    @Binds
    abstract fun bindApplication(application: App): Application

    @Binds
    @Singleton
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @ContributesAndroidInjector
    abstract fun contributeItemListActivityInjector(): MainActivity

    @Binds
    @IntoMap
    @ViewModelKey(MainScreenViewModel::class)
    abstract fun bindItemListViewModel(viewModel: MainScreenViewModel): ViewModel

    @ContributesAndroidInjector
    abstract fun contributeDetailsScreenInjector(): ProgramDetailsActivity

    @Binds
    @IntoMap
    @ViewModelKey(DetailScreenViewModel::class)
    abstract fun bindDetailsViewModel(viewModel: DetailScreenViewModel): ViewModel

}
