package keehoo.kree.com.recruitment.di

import dagger.Module
import dagger.Provides
import keehoo.kree.com.domain.repositories.RadioProgramRepository
import keehoo.kree.com.domain.usecases.GetAllRadioProgramsUseCase
import javax.inject.Singleton

@Module
class MainScreenModule {

    @Provides
    @Singleton
    fun provideGetRadioProgramsUseCase(repository: RadioProgramRepository): GetAllRadioProgramsUseCase {
        return GetAllRadioProgramsUseCase(repository)
    }
}
