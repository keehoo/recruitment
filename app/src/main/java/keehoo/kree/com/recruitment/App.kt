package keehoo.kree.com.recruitment

import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import keehoo.kree.com.recruitment.di.DaggerAppComponent

class App : DaggerApplication() {

    public override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().create(this)
    }

    override fun onCreate() {
        AndroidThreeTen.init(this)
        super.onCreate()
    }
}
