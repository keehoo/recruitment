package keehoo.kree.com.recruitment.recruitment.mainScreen.viewmodel

import com.kubickiengineering.ktx.addTo
import com.kubickiengineering.ktx.mutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import keehoo.kree.com.domain.entities.Program
import keehoo.kree.com.domain.usecases.GetAllRadioProgramsUseCase
import keehoo.kree.com.recruitment.base.ViewModelCoroutineBase
import keehoo.kree.com.recruitment.recruitment.mainScreen.viewmodel.MainScreenViewModel.MainScreenError.MainListLoadingError
import javax.inject.Inject

class MainScreenViewModel @Inject constructor(
        private val getProgrammsUseCase: GetAllRadioProgramsUseCase
) : ViewModelCoroutineBase() {

    val actions = com.kubickiengineering.ktx.SingleLiveEvent<Action>()
    val errors = com.kubickiengineering.ktx.SingleLiveEvent<MainScreenError>()
    val isLoading = mutableLiveData(false)
    private val latestPage = mutableLiveData<Int?>(null)
    private val totalPages = mutableLiveData<Int?>(null)
    val latestDataSet = mutableLiveData<List<Program>?>(emptyList())

    init {
        loadPage(1)
    }

    private fun loadPage(pageNumber: Int) {
        getProgrammsUseCase.execute(pageNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { isLoading.value = true }
                .doFinally { isLoading.value = false }
                .subscribeBy(
                        onSuccess = {
                            latestDataSet.value = latestDataSet.value?.plus(it.programs)
                            latestPage.value = it.pagination.page
                            totalPages.value = it.pagination.totalpages
                        },
                        onError = {
                            errors.value = MainListLoadingError
                        }
                ).addTo(disposables)
    }

    fun loadUpNextPage() = loadPage(1.plus(latestPage.value ?: 1))

    sealed class Action // TODO: may be used to notify view of view model actions.

    sealed class MainScreenError {
        object MainListLoadingError : MainScreenError() // TODO: hook up exception handling.
    }
}
