package keehoo.kree.com.recruitment.recruitment.mainScreen.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import keehoo.kree.com.domain.entities.Program
import keehoo.kree.com.recruitment.databinding.GenericItemLayoutBinding
import javax.inject.Inject

class RadioAdapter @Inject constructor() :
        RecyclerView.Adapter<ExportItemViewHolder>() {

    private val data = mutableListOf<Program>()

    var onClick: (Program, View, String) -> Unit = { a, b, c -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExportItemViewHolder =
            ExportItemViewHolder(GenericItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ExportItemViewHolder, position: Int) {

        holder.bind(data[position], position, onClick)
    }

    override fun getItemCount() = data.size

    fun setData(items: List<Program>) {
        if (items.notNullNorEmpty()) {
            data.clear()
            data.addAll(items)
            notifyDataSetChanged()
        }
    }
}

private fun <E> List<E>.notNullNorEmpty() = !this.isNullOrEmpty()

class ExportItemViewHolder(private val binding: GenericItemLayoutBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: Program, position: Int, onClick: (Program, ImageView, String) -> Unit) {

        ViewCompat.setTransitionName(binding.imageView, "transition$position")
        binding.viewModel = item
        binding.root.setOnClickListener { onClick(item, binding.imageView, "transition$position") }
        binding.executePendingBindings()
    }
}

