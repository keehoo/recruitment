package keehoo.kree.com.recruitment.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject
import kotlin.reflect.KClass

abstract class ActivityBase : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    fun <T : ViewModel> viewModelOf(viewModelClass: KClass<T>) =
            ViewModelProviders.of(this, viewModelFactory).get(viewModelClass.java)

}
