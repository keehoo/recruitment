package keehoo.kree.com.recruitment.base

object Utils {

    suspend fun <T> measureFunctionTime(block: suspend () -> T): Pair<T, Double> {
        val start = System.nanoTime()
        val result = block.invoke()
        val stop = System.nanoTime()
        return result to (stop - start) / 1.0e9
    }
}