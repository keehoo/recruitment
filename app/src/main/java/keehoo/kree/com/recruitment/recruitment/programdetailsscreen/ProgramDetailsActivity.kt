package keehoo.kree.com.recruitment.recruitment.programdetailsscreen

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import keehoo.kree.com.domain.entities.Program
import keehoo.kree.com.recruitment.R
import keehoo.kree.com.recruitment.base.ActivityBase
import keehoo.kree.com.recruitment.databinding.ActivityProgramDetailsBinding
import kotlinx.android.synthetic.main.activity_program_details.*

private const val SELECTED_PROGRAM_KEY = "keehoo.kree.com.oneg.recruitment.programdetailsscreen.SELECTED_PROGRAM_KEY"
private const val TRANSITION_NAME = "keehoo.kree.com.oneg.recruitment.programdetailsscreen.TRANSITION_NAME"

class ProgramDetailsActivity : ActivityBase() {

    private lateinit var vm: DetailScreenViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm = viewModelOf(DetailScreenViewModel::class)

        val program = (intent.getSerializableExtra(SELECTED_PROGRAM_KEY) as? Program)
        val transitionName = intent.getStringExtra(TRANSITION_NAME)
        vm.program.value = program

        DataBindingUtil.setContentView<ActivityProgramDetailsBinding>(
                this,
                R.layout.activity_program_details)
                .also {
                    it.lifecycleOwner = this@ProgramDetailsActivity
                    it.viewModel = vm
                }

        supportPostponeEnterTransition()

        if (transitionName != null) { imageView2.transitionName = transitionName }

        program?.let {
            Picasso.get()
                    .load(it.programimagetemplatewide)
                    .into(imageView2, object : Callback {
                        override fun onSuccess() { supportStartPostponedEnterTransition() }
                        override fun onError(e: Exception?) { supportStartPostponedEnterTransition() }
                    })
        }
    }

    companion object {
        fun start(activity: Activity, program: Program, toBundle: Bundle?, transitionName: String) {
            activity.startActivity(
                    Intent(activity, ProgramDetailsActivity::class.java)
                            .apply {
                                putExtra(SELECTED_PROGRAM_KEY, program)
                                putExtra(TRANSITION_NAME, transitionName)
                            }, toBundle)
        }
    }
}
