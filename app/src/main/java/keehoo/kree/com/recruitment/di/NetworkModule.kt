package keehoo.kree.com.recruitment.di

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.Reusable
import keehoo.kree.com.data.api.SwedishRadioApi
import keehoo.kree.com.data.repositories.RadioRepositoryImpl
import keehoo.kree.com.domain.repositories.RadioProgramRepository
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Reusable
    internal fun provideGson(): Gson = GsonBuilder()
            .serializeNulls()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES)
            .create()


    @Provides
    @Reusable
    internal fun provideOkHttpClient(): OkHttpClient =
            OkHttpClient
                    .Builder()
                    .readTimeout(40, TimeUnit.SECONDS)
                    .connectTimeout(40, TimeUnit.SECONDS)
                    .build()

    @Provides
    @Singleton
    internal fun provideRetrofitInterface(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl("https://api.sr.se/api/v2/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    internal fun provideRadioApi(retrofit: Retrofit): SwedishRadioApi {
        return retrofit.create(SwedishRadioApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideRadioProgrammRepository(swedishRadioApi: SwedishRadioApi): RadioProgramRepository {
        return RadioRepositoryImpl(swedishRadioApi)
    }
}
