package keehoo.kree.com.recruitment.base.ktx

import com.kubickiengineering.ktx.replace
import org.junit.Assert.assertTrue
import org.junit.Test

class ListKtxKtTest {

    @Test
    fun `should properly replace item in list`() {
        //given
        val originalList = listOf(1, 2, 3, 4, 5)
        val expectedList = listOf(99, 2, 3, 4, 5)

        //when
        val result = originalList.replace(99) { it == 1 }

        //then
        assertTrue("Item of value 1 should be replace by item of value 99", expectedList == result)
    }
}