package com.kubickiengineering.ktx


internal fun <E> MutableList<E>.setElements(list: List<E>): MutableList<E> {
    this.clear()
    this.addAll(list)
    return this
}

internal fun <T> List<T>.replace(newValue: T, block: (T) -> Boolean): List<T> {
    return map {
        if (block(it)) newValue else it
    }
}