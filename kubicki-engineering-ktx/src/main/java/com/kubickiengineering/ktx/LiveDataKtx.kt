package com.kubickiengineering.ktx


import androidx.annotation.MainThread
import androidx.lifecycle.*
import kotlin.reflect.KClass

inline fun <A, B> LiveData<A>.map(crossinline function: (A) -> B): LiveData<B> =
    Transformations.map(this) { function(it) }

inline fun <A, B> LiveData<A>.switchMap(crossinline function: (A) -> LiveData<B>): LiveData<B> =
    Transformations.switchMap(this) { function(it) }

fun <T> MutableLiveData<T>.default(value: T): MutableLiveData<T> = this.also { this.value = value }
fun <T> MediatorLiveData<T>.default(value: T): MediatorLiveData<T> = this.also { this.value = value }

fun <T> T.toMediatorLiveData(): MediatorLiveData<T> = MediatorLiveData<T>().default(this)
fun <T> T.toMutableLiveData(): MutableLiveData<T> = MutableLiveData<T>().default(this)
fun <T> T.toLiveData(): LiveData<T> = this.toMutableLiveData()

fun <T> mutableLiveData(initialValue: T): MutableLiveData<T> = MutableLiveData<T>().default(initialValue)
fun <T> mediatorLiveData(initialValue: T): MediatorLiveData<T> = MediatorLiveData<T>().default(initialValue)

fun <T> mergeLiveDataSources(vararg sources: LiveData<T>): LiveData<T> {
    val liveData = MediatorLiveData<T>()
    sources.forEach { source ->
        liveData.apply {
            addSource(source) { value = it }
        }
    }
    return liveData
}

fun <A, B> combineLatest(a: LiveData<A>, b: LiveData<B>): LiveData<Pair<A?, B?>> {
    return MediatorLiveData<Pair<A?, B?>>().apply {
        var lastA: A? = null
        var lastB: B? = null

        fun update() {
            this.value = Pair(lastA, lastB)
        }

        addSource(a) {
            lastA = it
            update()
        }
        addSource(b) {
            lastB = it
            update()
        }
    }
}

fun <A, B, C> combineLatest(a: LiveData<A>, b: LiveData<B>, c: LiveData<C>): LiveData<Triple<A?, B?, C?>> {
    return MediatorLiveData<Triple<A?, B?, C?>>().apply {
        var lastA: A? = null
        var lastB: B? = null
        var lastC: C? = null

        fun update() {
            this.value = Triple(lastA, lastB, lastC)
        }

        addSource(a) {
            lastA = it
            update()
        }
        addSource(b) {
            lastB = it
            update()
        }
        addSource(c) {
            lastC = it
            update()
        }
    }
}

fun <A, B, T> combineLatest(a: LiveData<A>, b: LiveData<B>, combine: (A?, B?) -> T): LiveData<T> = combineLatest(a, b).map { combine(it.first, it.second) }

fun <A, B> LiveData<A>.combineLatestWith(liveData: LiveData<B>) = combineLatest(this, liveData)
fun <A, B, T> LiveData<A>.combineLatestWith(liveData: LiveData<B>, combine: (A?, B?) -> T) = combineLatest(this, liveData, combine)

fun <D> LiveData<D>.triggeredBy(trigger: SingleLiveEvent<Any>) =
    combineLatest(this, trigger) { data, _ -> data!! }

fun <A, B> combineLatestNonNull(a: LiveData<A>, b: LiveData<B>): LiveData<Pair<A, B>> {
    return MediatorLiveData<Pair<A, B>>().apply {
        var lastA: A? = null
        var lastB: B? = null

        fun update() {
            val localLastA = lastA
            val localLastB = lastB
            if (localLastA != null && localLastB != null)
                this.value = Pair(localLastA, localLastB)
        }

        addSource(a) {
            lastA = it
            update()
        }
        addSource(b) {
            lastB = it
            update()
        }
    }
}

fun <A, B, T> combineLatestNonNull(a: LiveData<A>, b: LiveData<B>, combine: (A, B) -> T): LiveData<T> = combineLatestNonNull(a, b).map { combine(it.first, it.second) }

fun <T> LiveData<T>.filter(predicate: (T?) -> Boolean): LiveData<T> {
    val result = MediatorLiveData<T>()
    result.addSource(this) {
        if (predicate(it)) result.value = it
    }
    return result
}

@MainThread
inline fun <T> MutableLiveData<T>.applyChange(crossinline modifier: (T?) -> T) {
    value = modifier(value)
}

@MainThread
inline fun <T> MutableLiveData<T>.applyChangeIf(crossinline predicate: (T?) -> Boolean, crossinline modifier: (T?) -> T) {
    if (predicate(value)) applyChange(modifier)
}

inline fun <T> MutableLiveData<T>.postChange(crossinline modifier: (T?) -> T) {
    postValue(modifier(value))
}

inline fun <T> MutableLiveData<T>.postChangeIf(crossinline predicate: (T?) -> Boolean, crossinline modifier: (T?) -> T) {
    if (predicate(value)) postChange(modifier)
}

fun <In : Any, Out : In> LiveData<In>.filter(clazz: KClass<Out>): LiveData<Out> {
    return this.filter(clazz.java)
}

@Suppress("UNCHECKED_CAST")
private fun <In, Out : In> LiveData<In>.filter(clazz: Class<Out>): LiveData<Out> {
    return this.filter { clazz.isInstance(it) }
        .map { it as Out }
}

inline fun <reified Out> LiveData<*>.select(): LiveData<Out> {
    return this.filter { it is Out }
        .map { it as Out }
}

fun MutableLiveData<Boolean>.flip() = postChange { it != true }

/**
 * Predicate function to judge if two values are equal for distinction
 */
typealias LiveDataValueComparisonPredicate<T> = (T, T) -> Boolean

/**
 * Just simple object equality called on provided values
 */
private fun <T> equalityPredicate(): LiveDataValueComparisonPredicate<T?> = { previous, current -> previous != current }

// provide `distinctUntilChanged()` for whole types chain: LiveData <- MutableLiveData <- MediatorLiveData
fun <T> LiveData<T>.distinctUntilChanged(predicate: LiveDataValueComparisonPredicate<T?> = equalityPredicate()) = distinctUntilChanged(this, predicate) as LiveData<T>

fun <T> MutableLiveData<T>.distinctUntilChanged(predicate: LiveDataValueComparisonPredicate<T?> = equalityPredicate()) = distinctUntilChanged(this, predicate) as MutableLiveData<T>
fun <T> MediatorLiveData<T>.distinctUntilChanged(predicate: LiveDataValueComparisonPredicate<T?> = equalityPredicate()) = distinctUntilChanged(this, predicate) as MediatorLiveData<T>

/**
 * LiveData that propagates only distinct emissions.
 * source: https://gist.github.com/florina-muntenescu/fea9431d0151ce0afd2f5a0b8834a6c7#file-livedataextensions-kt
 */
private fun <T> distinctUntilChanged(
    liveData: LiveData<T>,
    predicate: LiveDataValueComparisonPredicate<T?>
): MediatorLiveData<T> {
    val distinctLiveData = MediatorLiveData<T>()
    distinctLiveData.addSource(liveData, object : Observer<T> {
        private var initialized = false
        private var lastObj: T? = null

        override fun onChanged(obj: T?) {
            if (!initialized) {
                initialized = true
                lastObj = obj
                distinctLiveData.postValue(lastObj)
            } else if ((obj == null && lastObj != null) || predicate(lastObj, obj)) {
                lastObj = obj
                distinctLiveData.postValue(lastObj)
            }
        }
    })
    return distinctLiveData
}

inline val <T> LiveData<T>.liveData: LiveData<T>
    get() = this
